def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board,index):
    print_board(board)
    print("Game Over")
    print(board[index], "has won")
    exit()


def is_row_winner(board,rowNum):
    if rowNum[0] == rowNum[1] and rowNum[1] == rowNum[2]:
        return True

def is_column_winner(board,columnNum):
    if columnNum[0] == columnNum[1] and columnNum[1] == columnNum[2]:
        return True

def is_diagonal_winner(board,diagonalNum):
    if diagonalNum[0] == diagonalNum[1] and diagonalNum[1] == diagonalNum[2]:
        return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"


for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    row1 = (board[0], board[1], board[2])
    row2 = (board[3], board[4], board[5])
    row3 = (board[6], board[7], board[8])
    columnL = (board[0], board[3], board[6])
    columnM = (board[1], board[4], board[7])
    columnR = (board[2], board[5], board[8])
    diagonalL = (board[0], board[4], board[8])
    diagonalR = (board[2], board[4], board[6])

    if is_row_winner(board,row1):
        game_over(board,0)
    elif is_row_winner(board,row2):
        game_over(board,3)
    elif is_row_winner(board,row3):
        game_over(board,6)
    elif is_column_winner(board,columnL):
        game_over(board,0)
    elif is_column_winner(board,columnM):
        game_over(board,1)
    elif is_column_winner(board,columnR):
        game_over(board,2)
    elif is_diagonal_winner(board,diagonalL):
        game_over(board,0)
    elif is_diagonal_winner(board,diagonalR):
        game_over(board,2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
